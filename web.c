#include <stdio.h>
#include <stdlib.h>
#include <libsocket/libinetsocket.h>
#include <ctype.h>
#include <string.h>

typedef struct {
    char * fname; 
    int fnum;
} FILELIST ;


FILE * connect_to_server();
void menu();
char get_choice();
void list_files(FILE *s);
void download(FILE *s);
void quit(FILE *s);
//void downloadAll(FILE *s);
//FILELIST * makeFArray(FILE *s, int *size);



int main()
{
    // Connect
    FILE * s = connect_to_server();
    //Get greeting line from server
    char line[1000];
    fgets(line,1000,s);
    
    while (1)
    {
    // Menu
    menu();
    
    // Get choice
    char choice = get_choice();
    
    // Handle choice
    switch(choice)
    {
        /* case 'a':
        case 'A':
            downloadAll(s);
            break; */
        case 'l':
        case 'L':
            list_files(s);
            break;
        
        case 'd':
        case 'D':
            download(s);
            break;
            
        case 'q':
        case 'Q':
            quit(s);
            exit(0);
            break;
            
        default:
            printf("Choice must be d, l, or q\n");
    }
    }
    
    
}

/*
 * Connect to server. Returns a FILE pointer that the
 * rest of the program will use to send/receive data.
 */
FILE * connect_to_server()
{
    int socknum = create_inet_stream_socket("runwire.com", "1234", LIBSOCKET_IPv4, 0);
    if(socknum == -1) {
        fprintf(stderr, "Couldn't connect\n");
        exit(1);
    }
    
    FILE *fp = fdopen(socknum, "r+");
    if(!fp) {
        fprintf(stderr, "Could not convert socket number\n");
        exit(2);
    }
    
    return fp;
}

/*
 * Display menu of choices.
 */
void menu()
{
    printf("[L] List files\n");
    printf("[D] Download a file\n");
    //printf("[A] Download all files\n");
    printf("[Q] Quit\n");
    printf("\n");
}

/*
 * Get the menu choice from the user. Allows the user to
 * enter up to 100 characters, but only the first character
 * is returned.
 */
char get_choice()
{
    printf("Your choice: ");
    char buf[100];
    fgets(buf, 100, stdin);
    return buf[0];
}

/*
 * Display a file list to the user.
 */
void list_files(FILE *s)
{
    
    fprintf(s,"LIST\n");
    //Getting +OK from server
    char line[1000];
    fgets(line,1000,s);
    
    char filename[100];
    unsigned long size;
    
    printf("%-10s%-20s\n", "Size", "Filename");
    printf("%-10s%-20s\n", "====", "========");
    
    while(fgets(line, 100, s) != NULL) {
        if (strlen(line) < 3) break;
        
        line[strlen(line)] = '\0';
        if (sscanf(line,"%lu %s", &size, filename) > 0) {
            printf("%-10lu%-20s\n", size, filename);
        }
        
    }
}

/*
 * Download a file.
 * Prompt the user to enter a filename.
 * Download it from the server and save it to a file with the
 * same name.
 */
void download(FILE *s)
{
    char line[1000];
    char filename[100];
    char str[100];
    int size;
    
    //Getting filename
    printf("Enter filename: ");
    scanf("%s%*c",filename);
    fprintf(s,"SIZE %s\n", filename); //calling for size
    //Getting return code
    fgets(line,1000,s);
    char msg[100];
    sscanf(line, "%s%d", msg, &size); //getting size
    
    //printf("%s",line); //test: prints +OK &size
    //printf(">>%d<<\n", size);
    
    if(line[0] == '+') {

        fprintf(s,"GET %s\n", filename);
        fgets(str, sizeof(str), s);
        //printf("str is: %s\n",str);
        
        FILE *dFile = fopen(filename, "wb+");
        if(!dFile) {
            printf("File %s cant be opened for writing\n", filename);
            exit(1);
        }
        
        unsigned char data[1000];
        int so_far=0;
        int got;
        int remainder;
        while(so_far != size ) {
            if (size - so_far > 1000) remainder = 1000;
            else remainder = size-so_far;
            got = fread(data, sizeof(unsigned char), remainder, s);
            
            fwrite(data, sizeof(unsigned char), got, dFile);
            so_far += got;
            
        }
        printf(">>>Download complete!<<<\n");
        fclose(dFile);
    }
    
}

/* void downloadAll(FILE *s)
{
    char filename[100];
    int fnumber;
    int size=0;
    
    FILELIST * files = makeFArray(s, &size);
    FILE *dFile;
}

FILELIST * makeFArray(FILE *s, int *size)
{
    fprintf(s, "LIST\n");
    char line[1000];
    fgets(line,1000,s);
    
    char filename[100];
    int fsize;
    FILELIST *farray = malloc( sizeof(FILELIST) * 11);
    
    char msg[100];
    
    int j=0;
    while(fgets(line,100,s) != NULL) {
        if(strlen(line) < 3) break;
        
        printf("line is %s\n", line); //size (space) fname


        if (sscanf(line,"%d%s", &fsize, filename ) >0) {
            farray[j].fname = filename;
            printf("name is %s\n", farray[j].fname );
            fprintf(s,"SIZE %s\n", farray[j].fname);
            sscanf(line, "%s%d", msg, &fsize); //msg holds +OK 
            j++;
        }
        printf("%d",j);
    }
    for(int i=0; i < strlen(farray); i++) {
        printf("%s", farray[i].fname);
    }
    return(farray);
} */

/* 
 * Close the connection to the server.
 */
void quit(FILE *s)
{
    fclose(s);
}
